var express = require('express');
var bodyParser = require('body-parser');
var sessions = require('express-session');
var app = express();
var db = require('./db');

var session;

app.set('view engine', 'jade');
app.set('views', './pages');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(sessions({
    secret: 'wg54hyy7jrherg4wrgG',
    resave: false,
    saveUninitialized: true
}));

app.get('/', function (req, res) {
    res.render('index', { title: 'Log in', message: 'Log in'});
});

app.get('/register', function (req, res) {
    res.render('register', { title: 'Register', message: 'Register'});
});
app.post('/register', function (req, resp) {
    session = req.session;
    db.register(req.body.username, req.body.password, function (err, res) {
        if (err) {
            console.error(err);
        } else if (res.error) {
            console.error(res.error);
        } else if (res.rowCount > 0) {
            session.uniqueID = req.body.username;
        }
        resp.redirect('/redirects');
    });
});
app.post('/login', function (req, resp) {
    session = req.session;
    db.login(req.body.username, req.body.password, function (err, res) {
        if (err) {
            console.error(err);
        } else if (res.error) {
            console.error(res.error);
        } else if (res.rows.length === 1) {
            session.uniqueID = res.rows[0].username;
        }
        resp.redirect('/redirects');
    });
});
app.get('/logout', function (req, res) {
    req.session.destroy();
    res.redirect('/');
});
app.get('/redirects', function (req, res) {
    session = req.session;
    if (session.uniqueID) {
        res.redirect('/admin');
    } else {
        res.end('Denied.');
    }
});

app.get('/admin', function (req, res) {
    session = req.session;
    if (session.uniqueID) {
        res.render('admin', {
            title: 'Admin',
            message: 'User: ' + req.session.uniqueID
        });
    } else {
        res.end('Denied.');
    }
});

app.use(function(req, res){
    res.sendFile(req.url, {root: __dirname});
});

app.listen(1337, function () {
    console.log('Listening at port 1337');
});
