var pg = require('pg');

var dbURL = 'postgres://suva:1234@localhost:5432/mydb';

var client = new pg.Client(dbURL);
client.connect();

var db = {
    login: function (username, password, cb) {
        client.query('SELECT * FROM suva.user WHERE username=$1 ' +
            'AND password=$2', [username, password], cb);
    },
    register: function (username, password, cb) {
        client.query('INSERT INTO suva.user(username, password) ' +
            'VALUES ($1, $2)', [username, password], cb);
    }
};

module.exports = db;
